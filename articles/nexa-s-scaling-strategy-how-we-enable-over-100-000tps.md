---
layout: layout.html
id: '009'
title: Nexa’s Scaling Strategy - How We Enable Over 100,000TPS
draft: false
description: There are a number of bottlenecks to allowing blockchain scaling on a
  PoW and UTXO-based network, and our plan is to knock them out the way one by one
  using mining incentives.
date: 13th December 2022
image: "/static/articles/nathan-watson-9l98kfbyiao-unsplash-1.jpg"
author: Paul Church
tags:
- Scaling

---
There are a number of bottlenecks to allowing blockchain scaling on a PoW and UTXO-based network but they can be summarised as:

* **Bandwidth**: how much data can be sent between nodes.
* **Speed**: at validating transactions.
* **Storage**: space to store the blockchain.

If any of these are constrained then massive scaling on-chain is impossible. In this article I’ll discuss Nexa’s strategy for opening up each of these bottlenecks and achieving a scale that will allow the world’s financial system to use Nexa as the backbone.

# Bandwidth

There are two main components to bandwidth usage on Nexa:

* **Initial Blockchain Download**: This is when a node downloads the entire history of the blockchain all the way back to genesis.
* **Receiving new transactions**: This is what allows the network to continually process new transactions as they are produced by wallets.

### Initial Blockchain Download

This IBD (initial blockchain download) is why it normally takes a long time to start up a new cryptocurrency full node. This process only increases in time as the blockchain gets longer and longer in a never-ending process. This historical blockchain data only increases in size as the number of transactions happening on the network increases. This creates a problem as it can mean it becomes impossible to ever download the full history of the network with even the fastest bandwidth connection. As of writing this, Nexa only has a blockchain of 107MB, which is of course very small as it has only just launched. Bitcoin by comparison has a blockchain size of 442GB and Ethereum is now over 1TB. Ethereum can only handle about 15 transactions every second and Bitcoin only 4. So, how to solve this?

By only ever needing the UTXO set and headers going back to the genesis Nexa is able to remove this huge bandwidth roadblock. The UTXO set is the set of all unspent transaction outputs. These are essentially the equivalent to coins, or rather ‘nuggets of value’ as I like to think of them. The UTXO set is every coin ever created on Nexa.

A header is a chunk of data embedded into each block listing some important details about that block. Each block header must reference the ID hash of the previous block, this therefore creates a chain of blocks connecting all the way back to the genesis block . You might even call it a…blockchain. Thanks to the data in each header, this also proves the total amount of Proof-of-Work it took to build that chain. This makes it easy to compare to other competing chains by simply comparing how much PoW was done in total on each.

There is a missing piece to this puzzle though. How will Nexa be able to get by with miners only ever needing the UTXO set instead of needing to download the full history first, as is the case with other UTXO-based blockchains? Well, this is where ‘UTXO Commitments’ technology comes into play. I’ll discuss how this works later in this article.

### Receiving New Transactions

Bandwidth has already been solved by B.U. using our [graphene technology](https://cryptoeconomics.cs.umass.edu/graphene/). It means that miners only require a small amount of bandwidth to announce a block and therefore do not cause orphans as you increase the block size. [Graphene](https://news.bitcoin.com/bitcoin-unlimited-merges-graphene-block-propagation-technology/) is a technology that significantly compresses the amount of data that miners must announce with their block solution. When miners announce a newly found block solution they must also tell other miners what transactions they have included in the block. For blocks with small numbers of transactions this is only a small amount of data, but as the number of transactions included in each block increases so does the size of blocks. But miners are receiving almost all the same transactions coming into their node as all other miners. That means they already have the information of the transactions and are being sent it a second time. Graphene reduces the required data to communicate what transactions are the block down to the bare minimum using some magic called IBLT. I’m not going to dive into that here, but [feel free to dive in yourself](https://www.youtube.com/watch?v=Syjj3SA95Tw&list=PL4IgwHUgjF5c90h-zRrGddm9Pg4jJ0rGA&index=3).

So essentially all bandwidth becomes sending transactions between nodes. 100,000 tps is about 20MB/sec which is trivial on a business data connection. Bandwidth speeds are also increasing all the time exponentially.

# Speed

This can be split into a number of parts, but the three main functions that take the most time when validating transactions are **SHA256 hashing**, **signature verification**, and **UTXO lookup**.

### SHA256 Hashing

SHA256 hashing is a key technology underlying most cryptocurrencies, including Nexa. A hash is a digital cryptographic fingerprint for a specific piece of information. There are a number of places in Nexa that hash functions are used, including in transactions and in the mining algorithm. The change in the financial and energy cost between now and the launch of Bitcoin is the key concept behind Nexa’s scaling strategy.

![](/static/articles/screenshot-2022-12-12-at-16-27-44.png)

Looking at this **logarithmic** chart of Bitcoin’s network hashrate is the best example of this scaling. In the first year of Bitcoin the network was capable of doing a total of a few million hashes per second with a single CPU being capable of perhaps ten thousand hashes per second or less, to today when the network is running 300 Exahashes per second and a single mining machine being capable of over 100TH/s. That is a 10,000,000,000x improvement in the speed of hashing per mining machine. It is unfathomable scaling.

These custom ASICs can be used to improve the speed of doing a SHA256 hash on your desktop computer 10 billion times, and nodes must do this operation for every transaction and often many times per transaction. By using customised hardware to speed up this operation, this bottleneck is also removed.

### Signature Verification

Signature verification is another compute-heavy operation that Nexa nodes must do at least once for every transaction. Cryptographic signatures are a key part of transactions and smart contracts on Nexa but they are a big bottleneck.

The NexScale POW forces miners to process both SHA256 hashes and Schnorr signatures. Those huge scaling incentives we have seen applied to bitcoin hashing will also now be applied to the Schnorr signatures found in Nexa. Over time this algorithm will be pushed into ever faster and more efficient hardware (i.e. FPGAs and eventually ASICs). [See this video for more details](https://www.youtube.com/watch?v=pDRCWcw5sAU).

### UTXO Lookup

The final part of Nexa’s scaling strategy is the UTXO lookup. This is also going to be put into the NexScale POW in a future protocol upgrade. This will force miners to do very fast lookups across the whole UTXO database to process the NexScale algo. Peter Rizun is already working on hardware to do this. See our resident Chief Scientist [discussing his ideas and prototypes for this on twitter](https://twitter.com/PeterRizun/status/1247554984968777729).

# Storage

## UTXO Commitments

BU is working on a new UTXO commitment upgrade which will mean that miners will only ever need the UTXO set instead of the full blockchain. This will make joining the network much faster and the growth of the UTXO size should be linear (especially with UTXO consolidation incentives we are considering). Solid state storage is decreasing in cost exponentially and a 1TB SSD has already achieved a cost of below \~$50. This is enough space for about 20 billion UTXOs. By the time we have achieved global adoption that $50 of space will more likely be 200-400 billion UTXOs and nodes would be able to afford much more than $50 for storage given the enormous transaction volume and fees they can collect. To put that in perspective, Bitcoin currently has about [80 million UTXOs](https://www.blockchain.com/explorer/charts/utxo-count) and is estimated to have about 100 million ‘’users’, so a 1TB SSD could handle 200x the size of UTXO set and therefore about 200 billion users. Or it could handle 8 billion users doing 25x more transactions each than Bitcoin currently handles per user. That's with today's technology and without any off-chain scaling tricks.

# A Nexa Monetary System

In the future we’ll do a much deeper dive into what a fully established Nexa monetary system would look like, but let’s have a little peek now.

Hardware will become available that will allow miners and businesses to operate a node at an initial hardware cost of less than $1000 and on a standard business internet connection. This will enable 100,000TPS using current technology and potentially much higher with future advances.

Not only will this allow the global population to access standard P2P payments, but it will unlock a whole new economy using our scalable smart contact technology…but that is a large topic for another article in the not too distant future.

Hopefully I’ve been able to provide some clarity on Nexa’s scaling strategy, a key component in our plans to finally bring crypto to the masses.