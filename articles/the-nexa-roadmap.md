---
layout: layout.html
id: '006'
title: The Nexa Roadmap
draft: false
description: Get the key details of Nexa's roadmap and the context that lead to it
  being born.
date: 14th November 2022
image: "/static/articles/mark-konig-ecgv8s2ipg0-unsplash.jpg"
author: Bitcoin Unlimited
tags:
- Roadmap

---
# Background

Before we dive into the Nexa roadmap, it’s important we establish the context in which the network was created, as this is what inspired us to create Nexa.

The problem being solved by Nexa is heavily derived from the original goal of Bitcoin. We want to provide the digital foundation for a permissionless financial system accessible to anyone in the world. The right to freely transact directly with people, is a human right, and is perhaps the most fundamental human right. Without the ability to spend money, you have no other rights.

Of course, there are now tens of thousands of other cryptocurrencies out there, so what makes Nexa stand out from the crowd? All existing crypto currencies have faced seemingly insurmountable roadblocks so far. These include having very limited functionality (e.g. Bitcoin BTC), poor economics (e.g. Luna), significant centralisation (e.g. Iota), and significant scaling limitations (e.g. Ethereum). No cryptocurrency has yet overcome all these problems simultaneously. We have mapped out a path for Nexa to overcome every one of these challenges in the near-term.

## The High-level Vision

### What Is The Technical Solution?

Nexa is a UTXO-based blockchain like Bitcoin. That means that ‘coins’ are transferred by moving nuggets of value between addresses and contracts. This is contrary to the ‘account’ model (e.g. in Ethereum) where balances on accounts are updated as funds are transferred. UTXO-based blockchains have many advantages over account-based blockchains, yet they have generally fallen out of favour with developers. We feel that many of the advantages and potential for UTXO-based blockchains are overlooked.

UTXO-based blockchains are naturally more scalable on layer-1, much easier for validating the security of transactions, and they are capable of much more advanced functionality than has so far been take advantage of. But we’re taking things even further.

Nexa currently operates on CPU (and soon GPU) mining, but the real magic happens when we leverage Nexa’s Proof-of-Work algorithm. The mining industry is incentivised to produce specialised hardware, such as FPGA’s and ASICs, that do more than just fast cryptographic hashing. Nexa’s PoW algorithm currently requires the use of cryptographic schnorr signatures. The same type of signature is found in Nexa transactions. This means that as the mining network gets ever more efficient, which it will as a highly competitive market, so will the network’s ability to produce and validate signatures. This is important, as a key scaling bottleneck is an ability for nodes to validate signatures. As the network grows, this bottleneck will simply fall away as an abundance of cheap hardware will be produced that can handle this.

In the mid-term, another scaling bottleneck will also be solved this way, UTXO lookups. An upgrade will be made to Nexa’s mining algorithm that will require miners to do fast access of the UTXO set. This will incentivise the creation of hardware to store and rapidly access the whole UTXO set.

With these two major bottlenecks out of the way, we expect Nexa nodes to easily handle 100,000 transactions per second, directly on layer-1, without any yet-to-be-developed advanced software. This is enough transactions for 8 billion people.

### What Is The Economic Solution?

Nexa uses the same tried and true economics of Bitcoin to incentivise and reward early adopters and provide ongoing mining security for network. Thanks to Nexa’s clear path to scale, this model also provides plenty of time to transition to a time when the majority of the miner’s revenue is coming from transaction fees. With just a 0.1 cent fee and 100,000 transactions per second, miners would have $8.6m in fees up for grabs every day.

One issue with Bitcoin’s economic model is the inelastic supply and therefore volatile price. Nexa offers native token systems of all types directly on-chain and backed by the full security of the proof-of-work system. This means there can be thousands, maybe millions, of different economic models implemented directly onto the network. Whether that be stablecoins, commodity-backed assets, NFTs or new types of asset never conceived, Nexa is ready for a thousand flowers to bloom. In doing so, we will find many economic systems that work great for all people.

### What Is The Social Solution?

We at Bitcoin Unlimited are big believers that leaders are a key factor in determining the nature of a community. We will provide strong leadership for the Nexa community and its values and will lead by example. We want to explicitly define what our values are.

* Maximise utility.
* Decentralised > centralised.
* HODL most but share some widely. (Network effects)
* Stay open to ideas.
* Don’t let perfect be the enemy of good.
* Find the right questions.
* Encourage truth discovery.
* Encourage collaboration.
* Reject toxicity and reward respectfulness.
* Pay people for their work.
* Narratives backed by reality.
* Hash power follows consensus.
* Price is important.
* Use proven tech where possible.
* Action > words.
* Tokenize everything.

# The Nexa Roadmap

#### _Next-gen financial services, with global-scale capacity, accessible to anyone._

<aside> 🪙 Core NEX & Token services.

</aside>

<aside> 💪 Gigablocks scaling.

</aside>

<aside> 💱 Exchange listings.

</aside>

<aside> 📢 Awareness.

</aside>

# Shipped

* Nexa node.
* Rostrum.
* Explorer.
* Wally wallet on Android.
* Nexa Script debug tool [debug.nexa.org](http://debug.nexa.org).
* Official social media accounts.
* [nexa.org](http://nexa.org/) (basic version)
* [spec.nexa.org](http://spec.nexa.org)

# Next Steps - Late-2022 to Mid-2023

## Core NEX & Token Functionality

* Tokens in explorer
* Tokens in Wally/libnexa.so
* Front-end redesign for Nifty
* Rostrum upgrades
* Nexa Mobile wallet - NEX functionality
* Example web app
* Fungible token tool page PoC (e.g. white label ticketing service)

## Gigablock Scaling Phase 2

* Signature FPGA
* UTXO Commitments
* FPGA miner
* Cashdrive

## Decentralized Token Exchange

* Prototype token DEX service
* Prototype Cross chain atomic swap service
* Constant product market maker research

## Nexa Awareness

* Focus on crypto users and developers.
* [Nexa.org](http://nexa.org/) (more educational info)
* Podcasts
* Educational video content
* Ads on social media test (youtube, twitter)
* Official social media account content
* Sponsored written content
* Community rewards
* Talks at conferences
* Twitter spaces
* Exchange listing

## PQC Sig Scheme Evaluation

* Evaluate possible schemes to make Nexa Quantum Computing resistant.

# Medium-term Actions - Mid-2023 to End-2024

## Business-orientated Token Infrastructure

### Nexa web/App SDK

* SDK to allow developers to implement.

## Fully-featured Token Services

* Ticketing
* Token generation & management

## Gigablock Scaling Phase 3

* Cashdrive (UTXO commitments)
* Final mining algo (HF2023)
* UTXO lookup (HF2023)
* Pool withholding defence (HF2023)
* Accelerated signature ASIC.

## Nexa Mobile wallet - token functionality

* Receive tokens
* Send Tokens
* Token history

## Major Marketing Push

* Conferences
* Hackathons
* Written content

# Long-term Actions - 2025 Onward

* Tailstorm.
* Upgrade UTXO bloating/shrinking incentives via fee policy.
* Read only UTXO Evaluation.
* Script Libraries.
* PQC Sig Scheme for Quantum resistance.
* UTXO-based DAO smart-contract research.