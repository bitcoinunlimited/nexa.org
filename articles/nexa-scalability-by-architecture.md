---
layout: layout.html
id: '003'
draft: false
title: Nexa Scalability by Architecture
description: The Nexa architecture has been designed to be more scalable than other
  blockchains in a number of ways. This is going to be the key to reaching the capacity
  needed for the world.
date: 4th August 2022
image: "/static/articles/nexa-scalability.jpg"
author: Andrew Stone
tags:
- Technology
- Scaling

---
## Overview

The Nexa architecture has been designed to be more scalable than other blockchains in a number of ways. This is going to be the key to reaching the capacity needed for the world.

## Do More With Less

One of the best ways to scale is to simply do less work. And the first scalability advantage I want to discuss does exactly that: In Nexa, scripts only need to be executed once (per validator), and can be executed at any time. This is a property of many Bitcoin-inspired blockchains, actually, although Bitcoin and others are so artificially limited in transaction space that the property is irrelevant. But since EVM-style machines are general purpose computing that can modify blockchain state arbitrarily, they must be executed sequentially in a well-known order to ensure that their state changes are applied consistently across every machine.

## Transparent Transactions

The reason scripts can be executed only once is due to Nexa’s transparent transactions discussed in a [previous post](https://www.nexa.org/news/nexa-s-transparent-transactions). As a reminder, the transparent transaction architecture (and in general most “UTXO”-style architectures) explicitly and immutably specify all blockchain records that will be accessed/deleted (and created). Scripts do not actually modify the blockchain when executed. Scripts only control whether the transaction is valid or not. Scripts also cannot access arbitrary blockchain data – they are limited to the data the transaction announces it is accessing (and deleting). Therefore, the transaction’s input data + scripts are completely deterministic – if the script returned “valid” yesterday it will return “valid” today. This means that scripts can be run when the transaction arrives, and do not need to be re-run during block creation.

How does a transaction ever become invalid then? The only way a transaction may become invalid is if one of the records it accesses does not exist in the blockchain. These are commonly called “inputs” because they bring blockchain data (and NEX and token balance) into the transaction’s control. This can happen because the input never existed, or because some other script used it. So although transaction scripts do not need to be re-executed, it _IS_ necessary to look up the records specified by the transaction inputs. And this brings us to the second scalability advantage.

## Embarrassingly Parallel

Transparent transactions allow massively parallel transaction processing and UTXO lookup. Using a transaction admission technology this author developed to demonstrate the feasibility of 1 gigabyte Bitcoin blocks during Bitcoin Unlimited’s infamous involvement in the “blocksize wars”, transaction inputs are passed through a bloom fast-filter before the transaction is evaluated in parallel with other pending transactions. A bloom filter is a very efficient way to discover possible duplicates – it can guarantee that a transaction’s inputs do not conflict with the inputs of any other transaction currently being evaluated. But standard bloom filters are a bit slow because they require a lot of hashing internally. However, the bloom filter variant, called a bloom fast-filter that I developed, does not have this problem. To develop this, I observed that the transaction inputs are already cryptographically-strong random data, so there is no need for a computationally expensive algorithm to randomize already random data. Instead, the fast-filter simply selects and permutes bits from this random input data so that an attacker cannot engineer collisions across the entire network.

This transaction admission pipeline allows the full power of modern multi-core machines to be put to work validating transactions. And Nexa’s algorithmically defined block size ensures that the maximum block size will grow when it is needed, rather than be mired in politics and conflicts of interest.

## Summary

In a final observation, nothing discussed above precludes sharding, rollups, sidechains or other technologies you might be reading about on other blockchains. These technologies all enable what are effectively multiple chains to work together in some fashion, since a single chain is limited by architectural choices that were not focused on scaling. But since Nexa’s fundamental blockchain was designed for massive scalability, imagine the capabilities when sharding this kind of chain!