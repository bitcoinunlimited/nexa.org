---
layout: layout.html
id: '010'
title: Nexa’s Supply Schedule - Using A Tried and True Model
draft: false
description: We get a lot of questions and misconceptions around Nexa’s supply schedule
  and economics. It’s important that there is clarity brought to this topic as fundamentally
  Nexa is aiming to be the foundation of a new economic system.
date: 19th December 2022
image: "/static/articles/nexa-supply.jpg"
author: Paul Church
tags:
- Economics

---
We get a lot of questions and misconceptions around Nexa’s supply schedule and economics. It’s important that there is clarity brought to this topic as fundamentally Nexa is aiming to be the foundation of a new economic system.

## Key Details

To put it simply, Nexa’s supply schedule maps directly to Bitcoin’s. 2.1 quadrillion (2,100,000,000,000,000) satoshis will have been mined in roughly 140 years time, or 21 trillion NEX. Right now, 10 million NEX are released every block as the block reward and these are published to the network roughly every 2 minutes. Roughly every 4 years the amount of NEX minted per block will be cut in half, until the final NEX is minted in roughly 140 years time.

Just to reiterate, this is essentially the exact same economic model that Bitcoin uses in-terms of it’s supply. Although the economics are actually quite different in-terms of the planned velocity of money and demand. More on that in another article.

## Isn’t There A Lot Of New Minted NEX Right Now?

What we are really talking about here is price, and the supply and demand that determines that price. Ok, but market dynamics are extremely complex, especially for commodity-like assets like Nexa. Newly minted supply is fixed at a constant rate by the protocol but miners are not in any way obligated to sell. They may feel that that it will be more profitable to hold the coin for longer. Equally, supply can come from coin holders deciding they want to sell.

“Sure, sure. But isn’t there a load of NEX being minted right now compared to how much has already been mined?”, I hear you asking. Well, let’s look at the numbers and the context of the network. There are roughly 1.5 trillion NEX in existence right now as of the date of this article, and there will be another 1.3 trillion mined in the next 6 months. Every year until the first halvening there will be roughly 2.6 trillion new NEX mined. So one way we can look at this is, ‘in the next 3 months, what percentage of NEX will be mined compared to all previously mined NEX’. This will give us a picture of how the supply will change relative to the existing supply over time.

![](/static/articles/screenshot-2022-12-19-at-13-47-15.png)

As can be seen from this graph, the percentage change per 3 months decreases rapidly over time from 50% now to about 6% in about 4 years time. This would seem to support the argument that there are currently a lot of new coins being minted, which will put a lot of downward pressure on price, right?

It’s not so simple. Remember, in the chart above we are just looking at supply. Another factor is demand. It’s important to keep in mind that Nexa being very new, means it has only reached a very small number of people…so far. What that means, is that we could potentially see the same (but opposite) dynamic in terms of new users and demand. Let’s have a look at a chart of what happens if we manage to grow our community at a linear rate from where it is now with 2000 members to 15,000 members in about 4 years from launch.

![](/static/articles/screenshot-2022-12-19-at-13-54-54.png)

As you can see, the chart looks identical. But the above is not actually what we are aiming for. What we are aiming for is an exponential growth in our community. We aim to leverage the community and our resources to reach ever larger numbers of people to grow the network rapidly. This chart looks very different…

![](/static/articles/screenshot-2022-12-19-at-14-05-25.png)

The above is what we are really aiming for. Massive, exponential growth like we have seen with Bitcoin.

But there is a huge factor that is missing in the above models, and that is price. The price itself places a huge role in how the supply affects the…price. Yep, that’s the true complexity of economics creeping in. The price recursively impacts the price.

## Price As A Factor of Price

So in the above model we have looked at how supply changes over time in Nexa in terms of NEX coins, but we haven’t looked at how the supply changes over time in terms of USD (my personal preference for pricing in that dirty fiat!). Now, of course there is no way to predict the price, and anyone telling you they can is lying to you. But what we can know for certain is that as the price increases, the amount of NEX supplied to the market in USD terms increases proportionally. As the price increases, it therefore takes an increasing amount of demand to eat up that supply. If the demand isn’t there to match the supply of a particular price point then the price will decrease.

Price impacts the supply in USD terms, which impacts the price, which impacts the supply in USD terms ad infinitum. This is a self-regulating feedback loop, and it’s the reason why its not possible to just arbitrarily pump the price of a well distributed and highly liquid coin with a non-dynamic supply schedule. The supply _will_ drive that price back down if the demand doesn’t jump up to match it. This is equally true in the opposite direction. Dumping the price of a coin down will ultimately be corrected by a reduction in supply in USD terms.

## Money Is a Social Network

An oft overlooked fact about monetary systems is that they are actually social networks, or more specifically socioeconomic networks. They allow us to transfer financial information between each other. One key property of networks, especially social networks, is Metcalfe’s law. Metcalfe’s law states that:

“A _network's impact is the square of the number of nodes in the network.”_

What this means, is that the more people join a network the more useful and therefore valuable it becomes. This is true for telephones, the internet, social media platforms, and it is true for cryptocurrencies. So, a huge factor in what determines the price of a cryptocurrency is the number of people who have some of it.

I see a lot of new crypto coin and token projects that try and skip to the end and either have no supply mechanism or a very fast one, where there is very little supply after the first year. I believe that in the long-term this actually harms the value of the network as it does not provide a constant stream of new coins into potential new investors hands, thereby increasing the utility and value of the network.

## In Summary

Ok, so I’ve said a lot of things about supply, demand and price but what does this all really mean? There is a certain amount of newly minted ‘value’ (fiat price * number of NEX) being minted every 2 minutes and the amount of people willing to come and buy up those new coins and any other coins available on the market will determine how price is impacted (either positively or negatively). From a starting point of a small absolute number of users and investors, it is comparatively ‘easy’ to increase demand to match or exceed the higher rate of early supply. This is how it was possible for Bitcoin to go from worthless to $30 per coin in 2011 with the same rate of supply that Nexa has now. There was a comparatively large jump in the number of people who found out about Bitcoin during that time, but as an absolute number the bitcoin investor-base only increased by a tiny fraction of the community we see today.

None of this is to say that the NEX price will go up (or down) at any specific rate, but that the currently supply of new coins will likely be a minor factor in comparison to the growth of the community.