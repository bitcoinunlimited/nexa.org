---
id: '001'
draft: false
title: Hello World - Nexa Is Finally Here!
layout: layout.html
description: Nexa is a next-generation cryptocurrency which launched on June 21st
  2022 and is a financial blockchain of programmable, packetized value. Unlike Bitcoin,
  which attempts to be a ‘digital gold’, or Bitcoin Cash which attempts to be ‘Digital
  Cash’, or Ethereum which attempts to be a ‘World Computer’, Nexa is a new blockchain
  which includes all the financial primitives to build the new global, web3 economy.
date: 14th July 2022
image: "/static/articles/rene-bohmer-yeuvdkzwsz4-unsplash-1.jpg"
author: Bitcoin Unlimited
tags:
- Announcement

---
## Background

The world of cryptocurrency has made a leaps of progress over the years. The initial launch of Bitcoin in 2009 provided the foundation for digital currencies and, thanks to Satoshi Nakamoto’s incredible initial design, it is still a leader within the crypto space today. But that doesn’t mean Satoshi Nakamoto got everything right. Historians of blockchain technology are more inclined to believe that Satoshi left Bitcoin abruptly, in the middle of development before all pieces of the puzzle had been put in place.

The next generations will need to learn from the mistakes made in the design of Bitcoin and the economic system surrounding it. Bitcoin has had some impact in the world but not enough. To achieve Bitcoin’s original goals it needs significant upgrades. This is where Nexa enters center stage!

## Nexa - The Financial Blockchain

Nexa is a next-generation cryptocurrency which launched on June 21st 2022 and is a financial blockchain of programmable, packetized value. Unlike Bitcoin, which attempts to be a ‘digital gold’, or Bitcoin Cash which attempts to be ‘Digital Cash’, or Ethereum which attempts to be a ‘World Computer’, Nexa is a new blockchain which includes all the financial primitives to build the new global, web3 economy.

The creators of Nexa, Bitcoin Unlimited, have taken their combined decades of deep experience in the blockchain industry, to craft an incredible new cryptocurrency that solves many of the issues in Bitcoin and pushes the technological boundaries.

### Fairly Launched, Commodity-style Coin

Nexa was fairly launched on June 21, 2022 after several months of testnet execution and notice was given in various media outlets and via a countdown timer at [www.nexa.org](http://www.nexa.org).

No ICO occurred, no coins are reserved or given to Bitcoin Unlimited or anyone else, either initially or as part of the mining reward.

Nexa’s initial coin distribution mechanism via miner rewards is extremely similar to Bitcoin’s which has been acknowledged by regulatory bodies as being a commodity, not a security.

### Scalability

Nexa makes use of the UTXO architecture found in Bitcoin to achieve massive scalability. Bitcoin Unlimited has already produced significant research showing how scaling to a capacity enough for the whole world is already within reach by using the UTXO model instead of the account model found in most EVM chains.

### Tokens

Nexa integrates token functionality directly into the base layer of the blockchain. This means tokens are first class citizens with all the same mining power that secures Nexa’s native coin. Token functionality isn’t just basic either. Token functionality gets full access to the advanced smart-contracting capabilities available in every transaction, so that you can create the next game-changing token or NFT project.

### Advanced Smart-contracts

New scripting functionality has also been introduced in Nexa, offering a major boost in Satoshi-family smart-contract capabilities without the scaling limitations found on EVM networks. Smart-contracts can now reference their own \[transaction\] data creating the possibility for exciting new functionality.

### Mining Algorithm

Nexa uses an innovative new mining algorithm not found in any other cryptocurrency. Nexa combines the traditional SHA-256 hashing algorithm with an elliptic curve multiplication, the same as is found in cryptographic signatures. The goal of this algorithm is to incentivize the development of hardware which can rapidly process cryptographic signatures in a highly efficient manner to remove one of the key bottlenecks to blockchain scalability.

## Where Are We Going?

We will continue to innovate at the blockchain layer, adding new primitives to make accessing the Nexa blockchain in a permissionless, trustless manner even easier. Additionally, we plan to focus on Layer 2 (but not Lightning style layer 2) anonymous, permissionless protocols that allow Nexa to realize its role as a facilitator of financial transactions.

We will deploy and help third parties deploy applications that use Nexa, and make the needed upgrades to infrastructure to support these applications.

## Come Join Us

Nexa is establishing itself as the financial foundation of the global economy. Come join us to be part of this journey. We need people like you to build out the applications and infrastructure needed to onboard the next billion users.

* [Matrix](https://matrix.to/#/#nexacoin:matrix.org)
* [Telegram](https://t.me/nexacoin)
* [Twitter](https://twitter.com/nexamoney)
* [Instagram](https://www.instagram.com/nexacoin)
* [TikTok](https://www.tiktok.com/@nexacoin)
* [Nexa.org](https://nexa.org)
* [Specification](https://spec.nexa.org)
* [Explorer](https://explorer.nexa.org)