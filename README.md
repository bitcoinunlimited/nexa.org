<div align="center">
  <h3>Gulp + Tailwind project skeleton</h3>
</div>

---

### Table of contents

- [Run the develoment server](#-run-the-develoment-server)
- [Contribute](#-contribute)
- [Deployment](#-deployment)

---

### 💻 Run the develoment server

You will need [nodeJS](https://nodejs.org/en/) installed on your machine, then run:

```shell
npm install
npm start
```

Use `ctrl + C` to stop the server.

---

### 👨🏻‍💻 Develop

##### Coding style

Use the coding style recommended by [codeguide.co](https://codeguide.co).

##### Structure

- HTML files are located in `/src/`. `.njk` files are templates ([Nunjucks](https://mozilla.github.io/nunjucks/)) files.

- CSS files are located in `/src/css/`.

- JavaScript files are located in `src/app.js`.

#### Components

Reusable components can be included in pages.

[Components documentation available here 🎉.](docs/components.md)

---

### 🚀 Deploy

Run `npm run build`. The `/dist/` folder contains the production website.

