# Components

**Reusable components can be included in pages, including articles. This page provides
documentation on how to use them.**

---

### Table of contents

- [Basic usage](#basic-usage)
- [All components](#all-components)
    - [Card](#card)
    - [Double paragraph](#double-paragraph)
    - [Hero header](#hero-header)
    - [Paragraph call to action](#paragraph-call-to-action)
    - [Paragraph image](#paragraph-image)

---

### Basic usage

1. Include components via the [`include`](https://mozilla.github.io/nunjucks/templating.html#include)
Nunjucks tag in any HTML file (`.html` or `.njk`).

For example:

```html
<section>
  {% include "components/hero-header.njk" %}
</section>
```

*Note that if you do not set the required data variables, placeholder data will be
used.*

![](img/example-placeholder.png)

2. Replace placeholder data by setting variables like so:

```html
<section>
  {% set componentTitle = "Global digital economy." %}
  {% set componentText = "Come join the revolution to build something that will last and age and change humanity’s path forever." %}
  {% set componentImage = "/static/articles/rene-bohmer-yeuvdkzwsz4-unsplash-1.jpg" %}
  {% set componentBtnText = "Let's go!" %}
  {% include "components/hero-header.njk" %}
</section>
```

![](img/example.png)


---

### All components

**We define here all the components sorted in alphabetical order, and their associated
variables.**

All components have examples defined below, with potential variants.

- [Card](#card)
- [Double paragraph](#double-paragraph)
- [Hero header](#hero-header)
- [Paragraph call to action](#paragraph-call-to-action)
- [Paragraph image](#paragraph-image)


#### Card

```html
<section>
  {% set componentTitle = "Discover Nexa For Games" %}
  {% set componentImage = "/static/img/cubes.png" %}
  {% set componentText = "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
  quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
  consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
  cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
  proident, sunt in culpa qui officia deserunt mollit anim id est laborum." %}
  {% include "components/card.njk" %}
</section>
```

Purple background:

```html
<section class="bg-white">
  {% set bg = "purple" %}
  {% set componentTitle = "Discover Nexa For Games" %}
  {% set componentImage = "/static/img/cubes.png" %}
  {% set componentText = "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
  quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
  consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
  cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
  proident, sunt in culpa qui officia deserunt mollit anim id est laborum." %}
  {% include "components/card.njk" %}
</section>
```

#### Double paragraph

```html
<section class="bg-white text-gray-700">
  {% set componentTitle1 = "Heading 1" %}
  {% set componentText1 = "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed
  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
  quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
  consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
  cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
  proident, sunt in culpa qui officia deserunt mollit anim id est laborum." %}
  {% set componentTitle2 = "Heading 2" %}
  {% set componentText2 = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur eget egestas sapien. Nam fringilla dui quis magna efficitur, id laoreet nunc dignissim. Morbi aliquam augue dui, in mattis justo dapibus ac. Donec tempor, lorem id.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur eget egestas sapien. Nam fringilla dui quis magna efficitur, id laoreet nunc dignissim. Morbi aliquam augue dui." %}

  {% include "components/double-paragraph.njk" %}
</section>
```


#### Hero header

```html
<section>
  {% set componentTitle = "The foundation of our future global digital economy." %}
  {% set componentText = "Come join the revolution to build something that will last and age and change humanity’s path forever." %}
  {% set componentImage = "/static/articles/rene-bohmer-yeuvdkzwsz4-unsplash-1.jpg" %}
  {% set componentBtnText = "Let's go!" %}
  {% include "components/hero-header.njk" %}
</section>
```


#### Paragraph call to action

```html
<section>
  {% set componentTitle = "HEADING 1" %}
  {% set componentText = "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
  quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
  consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
  cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
  proident, sunt in culpa qui officia deserunt mollit anim id est laborum." %}
  {% include "components/paragraph-cta.njk" %}
</section>
```


#### Paragraph image

```html
<section>
  {% set componentTitle = "Paragraph and image" %}
  {% set componentImage = "/static/articles/rene-bohmer-yeuvdkzwsz4-unsplash-1.jpg" %}
  {% set componentText = "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
  quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
  consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
  cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
  proident, sunt in culpa qui officia deserunt mollit anim id est laborum." %}
  {% include "components/paragraph-image.njk" %}
</section>
```

Inverted elements.

```html
<section>
  {% set orientation = "inverted" %}
  {% set componentTitle = "Paragraph inverted" %}
  {% set componentImage = "/static/articles/rene-bohmer-yeuvdkzwsz4-unsplash-1.jpg" %}
  {% set componentText = "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
  quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
  consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
  cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
  proident, sunt in culpa qui officia deserunt mollit anim id est laborum." %}
  {% include "components/paragraph-image.njk" %}
</section>
```
